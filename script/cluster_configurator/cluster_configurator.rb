require 'fileutils'
require 'enumerator'

def jboss_home
  `echo $JBOSS_HOME`.chomp
end

def prompt(default, *args)
  STDOUT.print(*args)
  result = gets.chomp
  result.empty? ? default : result
end

def server_ip_address
	# '192.168.1.14'
  `/sbin/ifconfig eth0`.match(/inet addr:(\d*\.\d*\.\d*\.\d*)/)[1]
end

def replace_in_file(path_to_file, regex, replacement_string)
  result = true
  begin
    text = File.read(path_to_file)    
    File.open(path_to_file, 'w') { |f| f.write(text.gsub(regex, replacement_string)) }
  rescue Exception => e
  	puts e
    result = false
  end
  result
end

def initial_hosts(master_ip_address, node_ip_addresses)
	node_initial_hosts = node_ip_addresses.collect{|s| "#{s}[7600]"}.join(',')
	"#{master_ip_address}[7600],#{node_initial_hosts}"	
end

def outbound_socket_bindings(master_ip_address, node_ip_addresses)
	node_bindings = node_ip_addresses.to_enum(:each_with_index).collect{|x,i| "#{outbound_socket_binding(x,(2+1)+i)}"}.join('')
	"#{outbound_socket_binding(master_ip_address,2)}#{node_bindings}"
end

def outbound_socket_binding(ip_address, position)
	"\n        <outbound-socket-binding name=\"messaging-server#{position}\">
          <remote-destination host=\"#{ip_address}\" port=\"5445\"/>
        </outbound-socket-binding>\n"
end

def generate_config_file(pwd, master_ip_address, node_ip_addresses, torquebox_version)

	done = false

	if torquebox_version == '2.0.3'

		FileUtils.rm    "#{pwd}/script/cluster_configurator/config_files/standalone-ha.xml", :force => true
		FileUtils.cp    "#{pwd}/script/cluster_configurator/standalone-ha.2.0.3.xml", "#{pwd}/script/cluster_configurator/config_files/standalone-ha.xml"

	  while !done
	    break if !replace_in_file("#{pwd}/script/cluster_configurator/config_files/standalone-ha.xml",/__INITIAL_HOSTS__/, initial_hosts(master_ip_address, node_ip_addresses))
	    break if !replace_in_file("#{pwd}/script/cluster_configurator/config_files/standalone-ha.xml",/__MOD_CLUSTER_CONFIG_PROXY_LIST__/, master_ip_address)
	    break if !replace_in_file("#{pwd}/script/cluster_configurator/config_files/standalone-ha.xml",/__OUTBOUND_SOCKET_BINDINGS__/, outbound_socket_bindings(master_ip_address, node_ip_addresses))
	    done = true
	  end
	elsif torquebox_version == '2.1.2'
	end

	done
end

pwd = "#{Dir.getwd}"

FileUtils.rm_r "#{pwd}/script/cluster_configurator/config_files", :force => true
FileUtils.mkdir "#{pwd}/script/cluster_configurator/config_files"

master_server_ip 	= prompt("#{server_ip_address}", "IP Address of 'Master' server in cluster [default is current server] : ")
number_of_nodes 	= prompt('1', "Number of nodes in cluster [1] [NOTE: the 'master' counts as a node] : ")

node_ip_addresses = []

for node_i in 1..number_of_nodes.to_i do
	node_ip_addresses << prompt("", "IP Address of 'Node-#{node_i}' server in cluster : ")
end

if generate_config_file(pwd, master_server_ip, node_ip_addresses, '2.0.3')
	puts "\nSuccessfully generated config file\n\n"

	puts "On 'master' and all 'node(s)' - execute the following ...\n\n"
	puts "source /root/.bash_profile ; cp #{pwd}/script/cluster_configurator/config_files/standalone-ha.xml #{jboss_home}/standalone/configuration/standalone-ha.xml"
	puts ""
	puts "Once 'standalone-ha.xml' file is replaced on all servers - reboot all servers ('master' first) or restart 'torquebox' service on each server"
else
	puts "ERROR generating config file"
end
