def execute_system_command(cmd, msg=nil, echo_cmd=false, verbose=false)
  STDOUT.puts msg if msg
  STDOUT.puts cmd if echo_cmd

  if verbose
    STDOUT.puts `#{cmd} > /dev/null 2>&1`
  else
    `#{cmd} > /dev/null 2>&1`
  end

  if $?.exitstatus == 0
    true
  else
    STDOUT.puts("ERROR executing cmd -> '#{cmd}'")
    false
  end
end

def get_environment_variable(name)
  result  = nil
  done    = false

  while !done
    cmd = "printenv | grep #{name}"
    str = `#{cmd}`.chomp

    if str != ''
      key_value_pairs = str.split("\n")
      key_value_pairs.each do |key_value_pair|
        tokens = key_value_pair.split('=')
        if tokens.length == 2 && tokens[0] == name
          result = tokens[1] 
          break
        end
      end
    end
    break
  end
  result
end

STDOUT.puts "\nDeploying application ...\n"

pwd  = `pwd`.chomp
done = false

while !done

  app_home = get_environment_variable("CATEGORIES_HOME")
  Dir.chdir(app_home)

  break if !execute_system_command("RAILS_ENV=production rake torquebox:deploy")
  
  STDOUT.puts "Successfully deployed CATEGORIES application"

  done = true
end

Dir.chdir(pwd)

if done
  puts "0: Success"
else
  puts "1: Failure"
end
