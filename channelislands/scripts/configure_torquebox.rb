require 'fileutils'

def server_ip_address
  `/sbin/ifconfig eth0`.match(/inet addr:(\d*\.\d*\.\d*\.\d*)/)[1]
end

def replace_in_file(path_to_file, regex, replacement_string)
  result = true
  begin
    text = File.read(path_to_file)    
    File.open(path_to_file, 'w') { |f| f.write(text.gsub(regex, replacement_string)) }
  rescue Exception => e
    result = false
  end
  result
end

def move_file(src_path, dest_path)
  result = true
  begin
    FileUtils.mv src_path, dest_path
  rescue Exception => e
    result = false
  end
  result
end

def execute_system_command(cmd, msg=nil, echo_cmd=false, verbose=false)
  STDOUT.puts msg if msg
  STDOUT.puts cmd if echo_cmd

  if verbose
    STDOUT.puts `#{cmd} > /dev/null 2>&1`
  else
    `#{cmd} > /dev/null 2>&1`
  end

  if $?.exitstatus == 0
    true
  else
    STDOUT.puts("ERROR executing cmd -> '#{cmd}'")
    false
  end
end

def prompt(default, *args)
  STDOUT.print(*args)
  result = gets.chomp
  result.empty? ? default : result
end

def configure_torquebox
  in_standalone = prompt('n', "Configure TorqueBox in 'standalone' or in 'cluster' mode [s ('standalone'), c ('cluster')] ? ").upcase
  
  if in_standalone == 'S'
    configure_torquebox_standalone
  else
    configure_torquebox_cluster
  end

  set_environment_variable('TORQUEBOX_CONFIGURED', 'TRUE')
end

def configure_torquebox_cluster
  result = false

  is_master = prompt('n', "Configure TorqueBox as a 'master' [y,N] ? ").upcase
  if is_master == 'Y' || is_master == 'YES'
    result = configure_torquebox_master
  else
    result = configure_torquebox_node
  end

  is_multicast = prompt('n', "\nDoes target network support multicasting [y,N] ? ").upcase

  if is_multicast == 'N' || is_multicast == 'NO'
    STDOUT.puts ""
    STDOUT.puts "Run 'cluster_configurator' script when installation of 'master' and all 'nodes' is complete"
    STDOUT.puts "In order to run 'cluster_configurator' - on 'master' or 'node' ..."
    STDOUT.puts "cd 'root' of DMW application (usually /opt/diva/www/:app_name)"
    STDOUT.puts "execute (and follow prompts) ..."
    STDOUT.puts "clear ; jruby script/cluster_configurator/cluster_configurator.rb"
    STDOUT.puts ""

    prompt('n', "\nUnderstood (hit enter/return to proceed) ? ")

  end
  result 
end

def configure_torquebox_standalone
  success = false
  proceed = prompt('n', "Has the Firewall and SELinux been disabled ? [y,N] ? ").upcase

  if proceed == 'Y' || proceed == 'YES'
    success = execute_system_command("service httpd start")

    STDOUT.puts "\ntorquebox run\n\n"
    # STDOUT.puts "RAILS_ENV='production' rake torquebox:deploy\n\n"
    
    prompt('n', "\nExecute the preceeding commands (in seperate terminal windows) when the installation is complete - understood ? [y,N] ")

  else
    STDOUT.puts "!!! Firewall and SELinux must be disabled in order for application to be 'served' !!!"
  end
  success
end

def configure_torquebox_master
  done    = false
  proceed = prompt('n', "Has the Firewall and SELinux been disabled ? [y,N] ").upcase

  if proceed == 'Y' || proceed == 'YES'
    master_hostname = prompt('tbx_master', "Hostname for 'master' ['tbx_master'] ? ")

    pwd   = `pwd`.chomp

    while !done
      break if !replace_in_file('/etc/sysconfig/network',/localhost.localdomain/, master_hostname)

      break if !execute_system_command("sed -i 's/$/ #{master_hostname}/' /etc/hosts")
      break if !execute_system_command("echo '#{server_ip_address}   localhost localhost.localdomain localhost4 localhost4.localdomain4 #{master_hostname}' >> /etc/hosts")
      break if !execute_system_command("/bin/hostname #{master_hostname}")    

      Dir.chdir('src')      

      break if !execute_system_command("tar xzvf mod_cluster-1.2.0.Final-linux2-x64-so.tar.gz")
      break if !execute_system_command("mv mod_advertise.so mod_manager.so mod_proxy_cluster.so mod_slotmem.so /etc/httpd/modules")
      break if !execute_system_command("chmod 755 /etc/httpd/modules/*.so")
      break if !execute_system_command("chown root:root /etc/httpd/modules/*.so")

      break if !replace_in_file('/etc/httpd/conf/httpd.conf',/LoadModule proxy_balancer_module modules\/mod_proxy_balancer.so/, '#LoadModule proxy_balancer_module modules/mod_proxy_balancer.so')
      break if !replace_in_file('/etc/httpd/conf/httpd.conf',/#ServerName www.example.com:80/, "ServerName #{server_ip_address}")
      break if !replace_in_file('mod_cluster.conf',/127.0.0.1/, server_ip_address)
      break if !move_file("#{`pwd`.chomp}/mod_cluster.conf", "/etc/httpd/conf.d/mod_cluster.conf")
      break if !execute_system_command("chown root:root /etc/httpd/conf.d/mod_cluster.conf")
      break if !execute_system_command("service iptables stop")
      break if !execute_system_command("chkconfig iptables off")
      break if !execute_system_command("service httpd start")
      break if !execute_system_command("chkconfig httpd on")

      STDOUT.puts "\nConfiguring TorqueBox to run as a service\n"
      break if !replace_in_file('torquebox.conf',/127.0.0.1/, server_ip_address)
      break if !execute_system_command("mkdir /etc/torquebox")
      break if !move_file("#{`pwd`.chomp}/torquebox.conf", "/etc/torquebox/torquebox.conf")
      break if !move_file("#{`pwd`.chomp}/torquebox", "/etc/init.d/torquebox")
      break if !execute_system_command("chmod 755 /etc/init.d/torquebox")
      break if !execute_system_command("chown root:root /etc/init.d/torquebox")
      break if !execute_system_command("service torquebox start")
      break if !execute_system_command("chkconfig torquebox on")

      done = true
    end

    Dir.chdir(pwd)

  else
    STDOUT.puts "!!! Firewall and SELinux must be disabled in order to proceed !!!"
  end
  done
end

def configure_torquebox_node
  done    = false
  proceed = prompt('n', "Has the Firewall and SELinux been disabled ? [y,N] ? ").upcase

  if proceed == 'Y' || proceed == 'YES'
    node_hostname = prompt('unknown', "Hostname for 'node' ? ")
    if node_hostname == 'unknown'
      while node_hostname == 'unknown'
        STDOUT.puts "\nPlease enter a hostname for this 'node'\n"
        node_hostname = prompt('unknown', "Hostname for 'node' ? ")
      end
    end

    pwd   = `pwd`.chomp

    while !done
      break if !replace_in_file('/etc/sysconfig/network',/localhost.localdomain/, node_hostname)

      break if !execute_system_command("sed -i 's/$/ #{node_hostname}/' /etc/hosts")
      break if !execute_system_command("/bin/hostname #{node_hostname}")    

      Dir.chdir('src')

      break if !execute_system_command("service iptables stop")
      break if !execute_system_command("chkconfig iptables off")
      break if !execute_system_command("service httpd start")
      break if !execute_system_command("chkconfig httpd on")

      STDOUT.puts "\nConfiguring TorqueBox to run as a service\n"
      break if !replace_in_file('torquebox.conf',/127.0.0.1/, server_ip_address)
      break if !execute_system_command("mkdir /etc/torquebox")
      break if !move_file("#{`pwd`.chomp}/torquebox.conf", "/etc/torquebox/torquebox.conf")
      break if !move_file("#{`pwd`.chomp}/torquebox", "/etc/init.d/torquebox")
      break if !execute_system_command("chmod 755 /etc/init.d/torquebox")
      break if !execute_system_command("chown root:root /etc/init.d/torquebox")
      break if !execute_system_command("service torquebox start")
      break if !execute_system_command("chkconfig torquebox on")

      done = true
    end
    
    Dir.chdir(pwd)
  else
    STDOUT.puts "!!! Firewall and SELinux must be disabled in order to proceed !!!"
  end
  done
end

def get_environment_variable(name)
  result  = nil
  done    = false

  while !done
    cmd = "printenv | grep #{name}"
    str = `#{cmd}`.chomp

    if str != ''
      key_value_pairs = str.split("\n")
      key_value_pairs.each do |key_value_pair|
        tokens = key_value_pair.split('=')
        if tokens.length == 2 && tokens[0] == name
          result = tokens[1] 
          break
        end
      end
    end
    break
  end
  result
end

def set_environment_variable(name, value)
  ENV[name] = value
  cmd = "echo 'export #{name}=#{value}' >> /root/.bash_profile"
  `#{cmd}`
end

def torquebox_configured?
  !get_environment_variable('TORQUEBOX_CONFIGURED').nil?
end

done = false
if torquebox_configured?
  done = true
  STDOUT.puts "TorqueBox already configured\n\n"
else

  while !done

    STDOUT.puts 'Configuring TorqueBox'

    break if !configure_torquebox
    
    STDOUT.puts "Successfully configured TorqueBox\n"

    done = true
  end

  STDOUT.puts "Error configuring TorqueBox" if !done
end

if done
  puts "0: Success"
else
  puts "1: Failure"
end

