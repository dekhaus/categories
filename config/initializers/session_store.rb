# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_categories_session',
  :secret      => '380064bbb01c58655883fe81d8f9475f706ef6ce6b9ceab8d364439cf640d3c4a06757a91bb4b0eddae68c77f2378c5349f2fa1adf4146eeef3b4232d3681ab5'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
